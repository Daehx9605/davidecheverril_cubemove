﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checker : MonoBehaviour
{
    private RaycastHit hit;
    private Vector3 maxBound;
    private Vector3 minBound;
    public bool GroundChecker()
    {
        bool isGround = false;

        if (Physics.Raycast(transform.position, -transform.up, out hit))
        {
            maxBound = hit.collider.bounds.max;
            minBound = hit.collider.bounds.min;
            isGround = true;
        }

        return isGround;
    }

    public Rotations RotateDirection(Transform _Player)
    {
        if (_Player.position.z >= maxBound.z)
        {
            return Rotations.Forward;
        }
        if (_Player.position.z <= minBound.z)
        {
            return Rotations.Back;
        }
        if (_Player.position.x >= maxBound.x)
        {
            return Rotations.Rigth;
        }
        if (_Player.position.x <= minBound.x)
        {
            return Rotations.Left;
        }
        return default;
    }
}