﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler 
{
    private Command cmdMove;
    private Command cmdRotate;
    private Checker Checker;

    public Command HandlerInput(Action_Move action)
    {
        switch (action)
        {
            case Action_Move.Move:
                cmdMove = new Behavioud_Move();
                return cmdMove;
            case Action_Move.Rotate:
                cmdRotate = new Behaviour_Rotation();
                return cmdRotate;
            default:
                return null;
        }
    }

}
