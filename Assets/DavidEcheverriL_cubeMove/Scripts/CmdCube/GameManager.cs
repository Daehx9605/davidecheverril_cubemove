﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject terrain;
    private int delayToRotate = 1;

    private void Start()
    {
        player.GetComponent<Controller_Actions>().rotateWorld += OnRotateWorld;
    }

    private void OnRotateWorld(Rotations rotation)
    {
        player.transform.SetParent(terrain.transform);
        Debug.Log(rotation);
        switch (rotation)
        {
            case Rotations.Forward:
                StartCoroutine(RotateAnim(-terrain.transform.right, 90f));
                break;
            case Rotations.Back:
                StartCoroutine(RotateAnim(terrain.transform.right, 90f));
                break;
            case Rotations.Rigth:
                StartCoroutine(RotateAnim(terrain.transform.forward, 90f));
                break;
            case Rotations.Left:
                StartCoroutine(RotateAnim(-terrain.transform.forward, 90f));
                break;
            case Rotations.No_Move:
                Debug.Log("Error");
                break;
        }
    }

    private IEnumerator RotateAnim(Vector3 referenceAxis, float finalAngle)
    {
        float valueToRotate = 0;
        while (valueToRotate < finalAngle)
        {
            terrain.transform.Rotate(referenceAxis, delayToRotate, Space.Self);
            valueToRotate += delayToRotate;
            yield return new WaitForSeconds(0.01f);
        }
        player.transform.SetParent(null);
        player.GetComponent<Controller_Actions>().ActivateCanMove();
    }
}
